# DFZ API

This NodeJS API is for DFZ JobLogger, DFZ Profile AND DFZ Store

This needs to be split apart, move profile sections to seperate api. JobLogger and store will be integrated together at some point

## Features

- Kubernetes, Docker
- Auth0 Authentication
- JWT Authentication for non-public use.
- Stripe Checkout
- MongoDB/Mongoose
- Redis Cache

## Image Upload

```bash
mkdir uploads && mkdir uploads/thumbs
```

## DotEnv

```env
    PORT=8080
    DB_URL=mongodb://dureplicaS
    DEBUG=true
    REDIS_PW=
    COOKIE_HASH='2'
    STRIPE_KEY='sk_test_'
    FIREBASE_KEY=''
    FIREBASE_PRIVATE_KEY_ID=""
    FIREBASE_PRIVATE_KEY="-----BEGIN PRIVATE KEY-----\-----END PRIVATE KEY-----\n"
    FIREBASE_CLIENT_EMAIL=""
    FIREBASE_CLIENT_ID=""
    FIREBASE_CLIENT_X509_CERT_URL=""

```

## Docker Build

```bash
    docker build -t "dufferzz/dfzapi:latest"
```

## Docker Run

```bash
    docker-compose up
```

## Kubernetes

Quickstart Minikube for testing

```bash
    minikube delete \
        && minikube start \
        && eval (minikube docker-env) \
        && docker build -t dufferzz/dfzapi:latest . \
        && minikube addons enable ingress \
        && kubectl create configmap redis-config --from-file=redis-config \
        && kubectl apply -f nginx.yaml \
        && kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.4/aio/deploy/recommended.yaml \
        && kubectl apply -f user.yaml \
        && echo "Sleeping 100s" \
        && sleep 100 && kubectl apply -f deployment.yaml \
        && sleep 5 && kubectl apply -f ingress.yaml \
        && kubectl describe pod dfzapi- && kubectl proxy & && sleep 1  \
        && google-chrome-stable http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/ \
        && kubectl -n kubernetes-dashboard describe secret (kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}') \
        && sleep 60s \
        && google-chrome-stable http://http://172.17.0.3:8080/v2/products/ \

```

## Proper route documentation **TODO**

## Nginx Config

/etc/nginx/sites-available/default

```nginx
upstream api{
    server 127.0.0.1:8080
}
```

/etc/nginx/sites-available/appname

```nginx
server {
    server_name appname;


location ^~ / {
    proxy_pass http://api;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
    proxy_set_header Host $host;
}

    listen 443 ssl; # managed by Certbot
    ssl_certificate  
    ssl_certificate_key  
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

```
