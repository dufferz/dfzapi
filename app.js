const restify = require('restify')

const mongoose = require('mongoose');
const CookieParser = require('restify-cookies');
const chalk = require('chalk');
const moment = require('moment');

const WebSocket = require('ws');

const {auth0Check, adminCheck} = require('./config/0auth');

const wss = new WebSocket.Server({
  port: 8081
});

wss.on('connection', function connection(ws, req) {
  ws.isAlive = true;
  ws.on('pong', heartbeat);
  ws.on('message', function message(msg) {
    console.log(`Received message: %o`, msg);
    msg = JSON.parse(msg)
    if (msg.message == "update"){
      console.log('Send update ws')

      wss.clients.forEach(function each(ws) {
        if (ws.isAlive === false) {
    
          console.log('Connection aborting..')
          return ws.terminate();
        }
        
      ws.send(JSON.stringify({
        message: 'DoUpdate'
    }));    
      });

  }
  });
});




const interval = setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if (ws.isAlive === false) {

      console.log('Connection aborting..')
      return ws.terminate();
    }
    ws.isAlive = false;
    ws.ping(noop);
  });
}, 10000);

wss.on('close', function close() {
  console.log('wss close')
  clearInterval(interval);
});

wss.on('error', function (err) {
  console.log('Error!', err)
});

function noop() {}

function heartbeat() {
  heartbeats.mark()
  this.isAlive = true;
}

const io = require('@pm2/io')
const meter = io.meter({
  name: 'Requests/sec',
  samples: 1,
  timeframe: 1
})
const heartbeats = io.meter({
  name: 'Heartbeats/min',
  samples: 60,
  timeframe: 60
})
const counter = io.counter({
  name: 'Total Page Requests'
})

const p = io.counter({
  name: 'Total Pings'
})

const config = require('./config');

const APIv2 = require('./routes/v2/routes.js')

const rjwt = require('restify-jwt-community');

const userController = require('./controllers/userController')

const morgan = require('morgan')

// Import ENV constiables from .env file
const dotenv = require('dotenv');
// const { admin } = require('firebase-admin/lib/database');
dotenv.config();


//Config Mongoose
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useFindAndModify', false); // Removes deprecation warning



console.log(chalk.blue(moment().format('h:mm:ss a')), 'Starting DFZ API');


// Connect to MongoDB
function connectDB() {
  try {
    console.log(chalk.blue(moment().format('h:mm:ss a')), 'Connecting to MongoDB...')
    mongoose.connect(process.env.DB_URL), {
      useNewUrlParser: true,
      useUnifiedTopology: true
    };
  } catch (error) {

    console.log(error)
    console.log('Reconnecting to MongoDB...')
    connectDB()
  };
}
connectDB()


// Handle Mongo Errors & Connections
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
  console.log(chalk.blue(moment().format('h:mm:ss a')), chalk.green('Connected to MongoDB Atlas'));
});

// Clean handle exits
process.on('SIGINT', function (err) {
  console.log(chalk.blue(moment().format('h:mm:ss a'), chalk.red('Caught SIGINT - TERMINATING PROCESS')));
  mongoose.disconnect();
  process.exit(err ? 1 : 0);
});

const server = restify.createServer();

// Set up CORS Headers
server.pre((req, res, next) => {
  // console.log(req.headers.origin)
  if (req.headers.origin == "http://localhost:4201"){
      res.header('Access-Control-Allow-Origin', 'http://localhost:4201');
      res.header('Access-Control-Allow-Headers', req.header('Access-Control-Request-Headers'));
      res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    
  } else if(req.headers.origin == "https://testing.dufferz.net") {
    res.header('Access-Control-Allow-Origin', 'https://testing.dufferz.net');
    res.header('Access-Control-Allow-Headers', req.header('Access-Control-Request-Headers'));
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
 
  } else {
      res.header('Access-Control-Allow-Origin', 'https://jobs.dufferz.net');

  }
  res.header('Access-Control-Allow-Headers', req.header('Access-Control-Request-Headers'));
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");

  // other headers go here..

  //pm2 metrics
  counter.inc()
  meter.mark()

  //debugging, i need to move to a non blocking logger....
  console.log(chalk.blue(moment().format('h:mm:ss a')), req.method + ' ' + req.url);

  if (req.method === 'OPTIONS') // if is preflight(OPTIONS) then response status 204(NO CONTENT)
    return res.send(204);
  next();
});


// server.use(morgan(':method :url :status :res[content-length] - :response-time ms'))

server.use(restify.plugins.bodyParser({
  "params": true
}));

server.use(restify.plugins.queryParser());

server.use(restify.plugins.multipartBodyParser()); // Enabling multipart for file uploads

server.use(restify.plugins.throttle({
  burst: 100,
  rate: 50,
  ip: true
}))


server.use(CookieParser.parse);


// Base Routes
server.get('/', function (req, res, next) {
  res.send(`You shouldn't be here!`)
});

// Serve static images files
server.get('/uploads/*', // don't forget the ``
  restify.plugins.serveStaticFiles('./uploads')
);

//Define Routes from Routes folder Here!!
//TODO: split v2 into seperate routes
APIv2.applyRoutes(server, '/v2');


// using restify-jwt to lock down everything except /auth
//const config = require('./config');
console.log(config.jwt)

server.use(rjwt(config.jwt).unless({
  path: [
    '/login',
    '/auth0test',
    '/applogin',
    '/v2/refreshToken',
    '/v2/verifyToken',
    '/logout',
    '/ping',
    '/private',
    /^\/uploads\/.*/,
    /\/attachment(.*)/,
    /^\/v2\/parts\/search\/name\/.*/,
    /^\/v2\/parts\/search\/number\/.*/,
    '/v2/products/',
    '/v2/categories/',
    '/v2/category/',
    '/v2/products/',
    /^\/v2\/categories\/.*/,
    /^\/v2\/category\/.*/,
    /^\/v2\/products\/.*/,

    /^\/v2\/payments\/.*/,
    /^\/v2\/stripe.*/,
    /^\/v2\/stripeorder.*/



  ]
}));

-
server.get('/ping', auth0Check, (req, res, next) => {
  // p.inc()
  res.send('pong');
});

server.get('/private', auth0Check, adminCheck, (req, res, next) => {
  res.send('fuk m8 u hax0r');
});

server.post('/login', (req, res, next) => {
  if (req.body && req.body.username && req.body.password) {
    const {
      username,
      password
    } = req.body
    userController.authenticate(username.toLowerCase(), password, req, res)
  } else {
    console.log(req)
    res.send(401, 'Are you dumb? Auth with something...')
  }
});

server.post('/applogin', (req, res, next) => {
  if (req.body && req.body.username && req.body.password) {
    const {
      username,
      password
    } = req.body
    userController.appAuth(username.toLowerCase(), password, req, res)
  } else {
    console.log(req.body)

    res.send(401, 'Are you dumb? Auth with something...')
  }
});

server.post('/logout', (req, res, next) => {
  if (req.cookies['token']) {
    console.log('Deleting Cookie!')
    res.clearCookie('token'); // Remove this old cookie
    // req.clearCookie('token')
    res.send('Logged Out Successfully')
  } else {
    console.log(`No cookie! :(`)
    res.send(`You are not logged in! No Token cookie found`)
  }
});



server.listen(process.env.PORT || '8080', function () {
  console.log(chalk.blue(moment().format('h:mm:ss a')), chalk.green('listening at', server.url));
});
