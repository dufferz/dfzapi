module.exports = {
    name: "DFZ_API",
    script: "./app.js",
    watch: false,
    //exec_mode: 'cluster',
    //instances: 2,
    PORT: 8080,
    "DEBUG_COLORS": true,
    "args": [ "--color" ]

};
