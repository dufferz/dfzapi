# FROM node:lts
FROM node:alpine
WORKDIR /app
COPY package*.json ./

# Move to Alpine linux for the sake of RasPi use, lighter weight

RUN apk add --no-cache --virtual .gyp python make g++ \
    && npm ci --only=production --silent \
    && apk del .gyp

# RUN npm install pm2 -g

COPY . .

EXPOSE 8080

CMD [ "node", "app.js" ]
