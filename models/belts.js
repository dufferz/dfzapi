const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//TODO: Come back and add more specific requirements

const beltSchema = new Schema({
  number: {
    type: String,
    required: false
  },
  name: {
    type: String,
    required: false
  },
  brand: {
    type: String,
    required: false
  },
  type: {
    type: String,
    required: false
  },
  leng: {
    type: String,
    required: false
  },
  profile: {
    type: String,
    required: false
  },
  price: {
    type: String,
    required: false
  },
  location: {
    type: String,
    required: false
  },

});

const belts = mongoose.connection.useDb('jobs');

const Belts = belts.model('belt', beltSchema);

module.exports = Belts;
