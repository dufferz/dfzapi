const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// TODO: Come back and add more specific requirements

// This Model is used on DFZ Profile

const personSchema = Schema({
  _id: Schema.Types.ObjectId,
  name: String,
  age: Number,
  stories: [{ type: Schema.Types.ObjectId, ref: 'Story' }]
});

const storySchema = Schema({
  author: { type: Schema.Types.ObjectId, ref: 'Person' },
  title: String,
  fans: [{ type: Schema.Types.ObjectId, ref: 'Person' }]
});

const dbb = mongoose.connection.useDb('jobs');


const Story = dbb.model('Story', storySchema);
const Person = dbb.model('Person', personSchema);

module.exports = {Story, Person}