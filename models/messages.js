const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//TODO: Come back and add more specific requirements

const beltSchema = new Schema({
  from: {
    type: String,
    required: false
  },
  to: {
    type: String,
    required: false
  },
  timeSent: {
    type: String,
    required: false
  },
});

const belts = mongoose.connection.useDb('jobs');

const Belts = belts.model('belt', beltSchema);

module.exports = Belts;
