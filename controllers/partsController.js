const Part = require('../models/parts');
const imagesModel = require('../models/attachments');
const dotenv = require('dotenv');
dotenv.config();

const ObjectId = require('mongodb').ObjectID;
const sharp = require('sharp');
const {
  clearKey
} = require("../config/cache");

const debug = require('debug')('server'),
  name = 'DFZ Repairs API';
const fs = require('fs');
const {
  uuid
} = require('uuidv4');



async function decode(uri) {
  const newUri = uri.split(';base64,').pop()
  const ext = uri.split('image/')[1]
  const extn = ext.split(';')[0]
  const buff = new Buffer(newUri, 'base64');
  const filename = uuid()
  const fullpath = `${savePath}/${filename}.${extn}`

  fs.writeFileSync(fullpath, buff);
  console.log(`Base64 image data converted to file: ${filename}.${extn}`);

  try {
    sharp(fullpath).resize(200, 200).toFile(`${savePath}/` + `${prefix}` + filename + '.' + extn, (err, resizeImage) => {
      if (err) {
        console.error(err);
      }
      else {
        console.log(`Image thumbnail created`);
        // console.log(resizeImage);
      }
    })
  } catch (error) {
    console.error(error);
  }

}
const savePath = "uploads"
const prefix = 'thumbnails-'

module.exports = {

  list: async function (req, res) {
    const list = await Part.find({}).limit(1000).sort({
      _id: -1
    }).cache({
      time: 600
    });
    res.send(list)
  },

  searchName: async function (req, res) {
    console.log('Searching for  %o', req.params.query)

    // Query length must me >2 characters

    if (req.params.query.length < 3) {
      res.send(500, 'Query Not Long Enough!')
    } else {
      const res1 = await Part.find({
        "partName": {
          //Regex to find * + regex + *
          $regex: new RegExp('.*' + req.params.query + '.*' + '$', "i")
          //const x = new RegExp('.*'+ req.params.query + '.*' + '$', "i")
        }
      });
      res.send(res1)
    }
  },

  searchNumber: async function (req, res) {
    console.log('Searching for  %o', req.params.query)

    if (req.params.query.length < 3) {
      res.send(500, 'Query Not Long Enough!')
    } else {
      const res1 = await Part.find({
        "partNumber": {
          $regex: `${req.params.query}.*`
        }
      });
      res.send(res1)
    }
  },

  paginate: async function (req, res) {
    // const list = await Part.find({}).limit(100).sort({
    //   _id: -1
    // }).paginate()

    if (typeof req.params.page == 'undefined') {
      req.params.page = 1
    }

    const options = {
      page: req.params.page,
      limit: 10,
      collation: {
        locale: 'en'
      }
    };

    const list = await Part.paginate({}, options).then({}) // Usage
    res.send(list)
  },

  //List all parts function

  listAll: async function (req, res) {
    // bugs in req query? 
    // const limit = req.query.limit.length > 0 ? req.query.limit : 1000
    const limit = 1000;

    const list = await Part.find({},
        ['partName', 'partNumber', 'price', 'thumbnail'])
      .limit(Number(limit))
      .sort({
        _id: -1
      })
      .cache({
        time: 600
      });
    // console.log(list.length) blocking!! not async
    res.send(list)
  },


  //Read Specific Part Information
  read: function (req, res, next) {
    const job = Part.findOne({
      _id: ObjectId(req.params.id)
    }, (err, data) => {
      if (err) {
        console.error(err)
        // return res.send('Job Not Found!');
      } else {
        return data
      }
    }).select("+images")

    res.send(job);
  },

  //Update Part
  update: async function (req, res, next) {
    console.log('Modifying part: %o', req.params.id)
    console.log(req.body)
    try {
      await Part.findOneAndUpdate({
        "_id": ObjectId(req.params.id)
      }, {
        $set: {
          partName: req.body.partName,
          barcode: req.body.barcode,
          stock: req.body.stock,
          Location: req.body.location,
          supplier: req.body.supplier,
          thumbnail: req.body.thumbnail
        }
      }, function () {
        res.send(`Modified Item ${req.params.id}`);
        clearKey('parts');
      });
    } catch (error) {
      debug(error);
      res.send(401, error);
    }


    // res.send(`Updated ${req.params.id}`)
  },

  //Append image to Part
  uploadImage: async function (req, res, next) {
    const obj = {
      'title': req.body.title,
      'image': req.body.image
    }

    console.log(obj)

    Part.findOneAndUpdate({
        _id: req.body.id
      }, {
        $push: {
          images: obj
        }
      },
      function (error, success) {
        if (error) {
          res.send(error.codeName)
          debug(error);
        } else {
          res.send('Success')
          clearKey('parts');
          decode(obj.image);
        }
      });
  },


  //Create parts
  create: function (req, res, next) {
    const {
      firstname,
      lastname,
      email,
    } = req.body;
    Part.create({
      firstname: firstname,
      lastname: lastname,
      email: email,
    }, (err, data) => {
      if (err) {
        debug(`Create error! ${err}`)
        return res.send(err);
      }

      const response = {
        message: `Successfully created ${data._id}`,
        id: `${data._id}`
      };
      clearKey('parts');
      return res.send(response);
    });
  },

  //Delete parts
  delete: function (req, res, next) {
    Part.findByIdAndRemove(req.params.id, (err, data) => {
      if (err) return res.send(500, err);
      const response = {
        message: `${req.params.id} ${data}`,
      };
      clearKey('parts');

      return res.send(response);
    });
  }
}