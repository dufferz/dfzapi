
const ObjectId = require('mongodb').ObjectID;
const sharp = require('sharp');

const imagesModel = require('../models/attachments');
const jobsModel = require('../models/job');

const fs = require('fs');
const {
  uuid
} = require('uuidv4');

function decode(uri) {
  const newUri = uri.split(';base64,').pop()
  const ext = uri.split('image/')[1]
  const extn = ext.split(';')[0]
  const buff = new Buffer(newUri, 'base64');
  const filename = uuid()
  const fullpath = `${savePath}/${filename}.${extn}`

  // TODO: Save path to DB here

  fs.writeFileSync(fullpath, buff);
  console.log(`Base64 image data converted to file: ${filename}.${extn}`);

  try {
    sharp(fullpath).resize(200, 200).toFile(`${savePath}/` + `${prefix}` + filename + '.' + extn, (err, resizeImage) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`Image thumbnail created`)
        // console.log(resizeImage);
      }
    })
  } catch (error) {
    console.error(error);
  }

}
const savePath = "uploads"
const prefix = 'thumbnails-'

module.exports = {

  list: async function (req, res) {
    console.log(`${req.reqUser} GET's ${req.url}`)
    const list = await imagesModel.find({}).limit().sort({
      _id: -1
    });
    // console.log(list)
    res.send(list)
  },
  
  read: async function (req, res, next) {
    const job = await jobsModel.find({
      _id: req.params.job
    }, (err, data) => {
      if (err) {
        console.log('Could not find user!')
        console.log(err)
        return res.send('Job Not Found!');
      } else {
        return data
      }
    }).select("images")
    // console.log(job)
    
    res.send(job[0]);
  },

  findUUID: async function (req, res, next) {
    console.log(`${req.reqUser}, ${req.params.UUID} GET's ${req.url}`)
    const list = await imagesModel.find({
      "UUID" : req.params.UUID
    }).limit().sort({
      _id: -1
    });
    console.log(list)
    res.send(list[0]['fileSrc'])
  },

  update: async function (req, res, next) {
    console.log(`${req.reqUser} tried to modify ${req.params.id}`)
    const {
      firstname,
      lastname,
    } = req.body;
    // console.log(parts)

    try {
      await imagesModel.findOneAndUpdate({
        "_id": ObjectId(req.params.id)
      }, {
        $set: {
          firstname: firstname,
          lastname: lastname,

        }
      }, function () {
        console.log(`Modified ${req.params.id}`)
        res.send(`Modified Item ${req.params.id}`);
      });
    } catch (error) {
      console.error(error);
      res.send(401, error);
    }


    // res.send(`Updated ${req.params.id}`)
  },

  uploadImage: async function (req, res, next) {
    console.log(`Image added to ${req.body.id} -- Length: ${req.body.image.length} -- Title: ${req.body.title}`)

    const obj = {
      'title': req.body.title,
      'image': req.body.image
    }

    imagesModel.findOneAndUpdate({
        _id: req.body.id
      }, {
        $push: {
          images: obj
        }
      },
      function (error, success) {
        if (error) {
          res.send(error.codeName)
          console.log(error);
        } else {
          // console.log(success);
          res.send('Success')
          decode(obj.image);
        }
      });
  },
  create: function (req, res, next) {
    console.log(`${req.reqUser} tried to create a job`)
    console.log(req.body)
    const {
      firstname,
      lastname,
      email,
    } = req.body;
    console.log(firstname, lastname)
    imagesModel.create({
      firstname: firstname,
      lastname: lastname,
      email: email,
    }, (err, data) => {
      if (err) {
        console.log(`Create error! ${err}`)
        return res.send(err);
      }

      const response = {
        message: `Successfully created ${data._id}`,
        id: `${data._id}`
      };
      console.log(`${req.reqUser} SUCCESSFULLY created ${data._id}`)
      return res.send(response);
    });
  },

  delete: function (req, res, next) {
    console.log(`${req.reqUser} deleted ${req.params.id} `)
    imagesModel.findByIdAndRemove(req.params.id, (err, data) => {
      if (err) return res.send(500, err);

      const response = {
        message: `${req.params.id} ${data}`,
      };
      return res.send(response);
    });
  }
}