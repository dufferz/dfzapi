const Products = require('../models/products');
const StoreOrders = require('../models/storeOrders');

// Import ENV Variables from .env file
const dotenv = require('dotenv');
dotenv.config();

const {
  ObjectID
} = require('mongodb');

const {
  clearKey
} = require("../config/cache");

const stripe = require('stripe')(process.env.STRIPE_KEY);

//Generate items array in a way that stripe will accept

function GenerateItems(itemss) {
  let items = []
  itemss.forEach(item => {
    items.push({
      'price_data': {
        'currency': 'NOK',
        'product_data': {
          'name': `${item.name}`
        },
        'unit_amount': item.price * 100 // need to fix this. stripe uses smallest increment for money
      },
      'quantity': item.qty
    })
  });
  return items
}


module.exports = {
  read: async function (req, res, next) {
    let items
    let ino = req.body.id || '';

    try {
      items = await StoreOrders.findOne({
        checkoutID: ino,
        customerID: req.body.user
      }).catch((err) => {
        console.error(err)
      })
    } catch (error) {}

    if (typeof items != null && items != null) {
      // console.log(items)

      res.send({
        items: items
      })
    } else {
      res.send({
        status: 'error',
        data: `No orders found with ID: ${ino} for current user`
      })
    }
  },
  readOne: async function (req, res, next) {
    let items, err

    let ino = req.body.id || '';

    try {
      items = await StoreOrders.findOne({
        order_ID: ino
      }).sort({
        '_id': '-1'
      })
      // res.send(items)
    } catch (error) {
      console.error(error)
    }

    if (typeof items != null) {
      res.send({
        // session: session,
        items: items
      })
    } else {
      res.send({
        status: 'error',
        data: 'Error: No results'
      })
    }
  },
  readUser: async function (req, res, next) {
    let items

    const ino = req.body.user || '';

    try {
      items = await StoreOrders.find({
        customerID: ino
      }).sort({
        '_id': '-1'
      })
      // res.send(items)
    } catch (error) {
      console.error(error)
    }

    if (typeof items != null) {
      res.send({
        items: items
      })
    } else {
      res.send({
        status: 'error',
        data: 'Error: No results'
      })
    }
  },


  readAll: async function (req, res) {
    const sessions = await StoreOrders.find().sort({
      '_id': '-1'
    })
    if (typeof sessions != null) {
      res.send({
        sessions: sessions
      })
    } else {
      res.send({
        status: 'error',
        data: 'Error: No results'
      })
    }

  },

  readCustomer: async function (req, res) {
    if (req.params.id) {

      const customer = await stripe.customers.retrieve(
        req.params.id
      );
      res.send(customer)
    } else {
      res.send({
        status: 'error',
        data: 'Error: No results'
      })
        }
  },

  readIntent: async function (req, res) {
    if (req.params.id) {

      const paymentIntent = await stripe.paymentIntents.retrieve(
        req.params.id
      );

      res.send(paymentIntent)
    } else {
      res.send({
        status: 'error',
        data: 'Error: No results'
      })
        }
  },


  createOrder: async function (session, eventID) {

    let orders = await StoreOrders.find({
      eventID: eventID
    })

    console.log(orders.length)

    if (orders.length >= 1) {
      console.log('already in database!')
    } else {

      // console.log(session)
      console.log('Creating Order..')
      const items = await stripe.checkout.sessions.listLineItems(
        session.id)

      const paymentIntent = await stripe.paymentIntents.retrieve(
        session.payment_intent
      );

      const risklevel = paymentIntent.charges.data[0].outcome.risk_level
      const riskscore = paymentIntent.charges.data[0].outcome.risk_score
      
      StoreOrders.create({
        checkoutID: session.id,
        customerID: session.customer,
        paymentID: session.payment_intent,
        eventID: eventID,
        orderTotalPrice: session.amount_total,
        deliveryInfo: session.shipping.address,
        customerName: session.shipping.name,
        customerEmail: paymentIntent.receipt_email,
        customerPhone: session.client_reference_id,
        items: items.data,
        dateCreated: new Date(),
        riskLevel: risklevel,
        riskScore: riskscore,
        paymentAmountReceived: paymentIntent.amount_received,
        orderStatus: 'received',
        deliveryStatus: 'unscheduled',
        delivered: false,
        refunded: false,
        paymentType: paymentIntent.payment_method_types[0] || 'stripe'
      }).catch((err) => {
        console.error(err)
      }), (err, data) => {
        if (err) {
          console.log(`Create error! ${err}`)
          // return res.send(err);
        } else {
          console.log('Order added:', data._id)
          // console.log('data', data)
        }
      }
    }
  },


  create: async function (req, res) {
    if (req.body.items && req.body.id && req.body.phone) {

      const items = await GenerateItems(req.body.items)
      let session, err;
      try {
        session = await stripe.checkout.sessions.create({
          payment_method_types: ['card'],
          line_items: items,
          customer: req.body.id,
          mode: 'payment',
          client_reference_id: req.body.phone,
          billing_address_collection: 'auto',
          shipping_address_collection: {
            allowed_countries: ['NO'],
          },
          success_url: 'https://testing.dufferz.net/payment-success?session_id={CHECKOUT_SESSION_ID}',
          cancel_url: 'https://testing.dufferz.net/payment-cancelled',

        });
      } catch (error) {

        err = error
        console.error(error)
      }


      if (typeof session != "undefined" && typeof err != "string") {

        res.send({
          status: 'authorized',
          intent: session
        })
        // createOrder(session)
      } else {
        res.send({
          status: 'error',
          data: 'Invalid User ID. Is your account verified?<br>Please Check your Email!'
        })
      }

    } else {
      res.send({
        status: 'error',
        data: 'No Details Supplied'
      })
    }
  }
}