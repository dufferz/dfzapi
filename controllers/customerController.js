const customerModel = require('../models/customers');

const ObjectId = require('mongodb').ObjectID;

const debug = require('debug')('server')
module.exports = {

  list: async function (req, res) {
    const list = await customerModel.find({}).limit(200).sort({
      _id: -1
    });
    res.send(list)
  },

  listAll: async function (req, res) {
    const limit = 3000;
    const list = await customerModel.find({},['partName','partNumber','price','thumbnail']).limit(Number(limit)).sort({
      _id: -1
    });
    console.log(list.length)
    res.send(list)
  },

  read: function (req, res, next) {
    const job = customerModel.findOne({
      _id: ObjectId(req.params.id)
    }, (err, data) => {
      if (err) {
        console.log('Could not find user!')
        console.log(err)
        // return res.send('Job Not Found!');
      } else {
        return data
      }
    }).select("+images")

    res.send(job);
  },

  update: async function (req, res, next) {
    const {
      firstname,
      lastname,
    } = req.body;
    // console.log(parts)

    try {
      await customerModel.findOneAndUpdate({
        "_id": ObjectId(req.params.id)
      }, {
        $set: {
          firstname: firstname,
          lastname: lastname,

        }
      }, function () {
        res.send(`Modified Item ${req.params.id}`);
      });
    } catch (error) {
      debug(error);
      res.send(401, error);
    }


    // res.send(`Updated ${req.params.id}`)
  },

  create: function (req, res, next) {
    const {
      firstname,
      lastname,
      email,
    } = req.body;
    customerModel.create({
      firstname: firstname,
      lastname: lastname,
      email: email,
    }, (err, data) => {
      if (err) {
        debug(`Create error! ${err}`)
        return res.send(err);
      }

      const response = {
        message: `Successfully created ${data._id}`,
        id: `${data._id}`
      };
      return res.send(response);
    });
  },

  delete: function (req, res, next) {
    customerModel.findByIdAndRemove(req.params.id, (err, data) => {
      if (err) return res.send(500, err);
      const response = {
        message: `${req.params.id} ${data}`,
      };
      return res.send(response);
    });
  }
}