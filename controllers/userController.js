const User = require('../models/user');
// Import ENV constiables from .env file
const dotenv = require('dotenv');
dotenv.config();

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = {


    updateToken: async function (req, res, next) {
        console.log('Modifying User: %o', req.params.user)
        console.log(req.body.token)
        try {
            await User.findOneAndUpdate({
                "username": req.params.user
            }, {
                $set: {
                    firebaseToken: req.body.token,
                }
            }, function (data) {
                console.log(data)
                res.send(`Modified User ${req.params.user}`);
                // clearKey('parts');
            });
        } catch (error) {
            console.log(error);
            res.send(401, error);
        }
    },


    list: async function (req, res) {
        const list = await User.find({}).limit().sort({
            _id: -1
        }).select('-password')
        res.send(list)
    },


    addTask: async function (req, res) {
        console.log(req.body)

        const newtask = {
            task: req.body.task,
            completed: false
        }

        console.log(newtask)

        try {
            await User.findOneAndUpdate({
                _id: req.params.user
            }, {
                $push: {
                    tasks: newtask
                }
            }, function (data) {
                console.log(data)
                res.send(`Modified Users tasks ${req.params.user}`);
            });
        } catch (error) {
            console.log(error);
            res.send(401, error);
        }
    },


    getTasks: async function (req, res) {
        const list = await User.findById(req.params.user).limit().sort({
            _id: -1
        }).select('tasks')
        try {

        } catch (error) {

        }
        console.log(list)
        res.send(list)
    },

    create: async function (req, res, next) {
        console.log(req.body)
        User.create({
            fullname: req.body.fullname,
            username: req.body.username,
            password: req.body.password,
            role: req.body.role,
            email: req.body.email
        }, (err, data) => {
            if (err) {
                console.log(`Create error! ${err}`)
                return res.send(err);
            }

            const response = {
                message: `Successfully created ${data._id}`,
                id: `${data._id}`
            };
            //clearKey('users');
            return res.send(response);
        });

        console.log('?')

    },

    delete: async function (req, res, next) {
        console.log(`Deleting user ${req.params.id}`)
        await User.findByIdAndRemove(req.params.id, (err, data) => {
            if (err) return res.send(500, err);
            const response = {
                message: `${req.params.id} ${data}`
            };
            return res.send(200, response);
        });
    },
    authenticate: async function (username, password, req, res, next) {
        User.findOne({
            username: req.body.username
        }, function (err, userInfo) {
            if (err) {
                next(err);
            } else if (userInfo) {
                // console.log(userInfo)
                if (bcrypt.compareSync(password, userInfo.password)) {
                    const token = jwt.sign({
                        id: userInfo._id,
                        'username': userInfo.username,
                        'role': userInfo.role,
                    }, process.env.COOKIE_HASH, { // Define SECRET JWT KEY
                        expiresIn: '12h'
                    });
                    //   console.log('set cookie', token)
                    res.setCookie('token', token, {
                        path: '/',
                        domain: 'dufferz.net',
                        maxAge: 14200,
                        secure: true,
                        httpOnly: false
                    });
                    // Last Login time update

                    try {
                        User.findOneAndUpdate({
                            "username": req.body.username
                        }, {
                            $set: {
                                lastLogin: new Date(),
                            }
                        }, function (data) {

                        });
                    } catch (error) {
                        debug(error);
                        res.send(401, error);
                    }
                    res.send('Successfully Authenticated!');
                } else {
                    res.send(401, 'Error authenticating!');
                }
            } else {
                res.send(401, 'Error authenticating!');
            }
        });
    },

    //Essential duplicate of above
    appAuth: async function (username, password, req, res, next) {
        User.findOne({
            username: req.body.username
        }, function (err, userInfo) {
            if (err) {
                next(err);
            } else if (userInfo) {
                // console.log(userInfo)
                if (bcrypt.compareSync(password, userInfo.password)) {
                    const token = jwt.sign({
                        id: userInfo._id,
                        'username': userInfo.username,
                        'role': userInfo.role,
                    }, process.env.COOKIE_HASH, { // Define SECRET JWT KEY
                        expiresIn: '12h'
                    });
                    res.setCookie('token', token, {
                        path: '/',
                        domain: 'dufferz.net',
                        maxAge: 14700,
                        secure: true,
                        httpOnly: false
                    });
                    try {
                        User.findOneAndUpdate({
                            "username": req.body.username
                        }, {
                            $set: {
                                lastLogin: new Date(),
                            }
                        }, function (data) {
                            console.log(data)
                        });
                    } catch (error) {
                        debug(error);
                        res.send(401, error);
                    }
                    res.send({status:'Successfully Authenticated!', username: userInfo.username, token: token});
                } else {
                    res.send(401, 'Error authenticating!');
                }
            } else {
                res.send(401, 'Error authenticating!');
            }
        });
    },
    verifyToken: async function (username, password, req, res, next) {
        console.log(req)
    },
    refreshToken: async function (username, password, req, res, next) {
        console.log(req)
    },
}