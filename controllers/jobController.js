const Job = require('../models/job');
const Image = require('../models/attachments');

const ObjectId = require('mongodb').ObjectID;
const sharp = require('sharp');

const pdf = require("pdf-thumbnail")
const im = require('imagemagick');
const PDFImage = require("pdf-image").PDFImage;

// Connect to FireBase
const admin = require("firebase-admin");
const serviceAccount = require("../config/dfzapi-firebase-adminsdk-ih8ag-806c1f6c57.js");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://dfzapi.firebaseio.com"
});
const {
  clearKey
} = require("../config/cache");


const fs = require('fs');
const {
  uuid
} = require('uuidv4');

const savePath = "uploads"
const prefix = 'thumbnails-'

function dataURLtoFile(dataurl, filename) {

  const arr = dataurl.split(','),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }

  return new File([u8arr], filename, {
    type: mime
  });
}

async function decode(res, jobID, obj) {

  const originalName = obj.originalName
  const title = obj.title
  const uploader = obj.uploader
  const fileSize = obj.fileSize

  // TODO: Need to handle split returning nothing because invalid input

  const newUri = obj.uri.split(';base64,').pop()
  const ext = obj.uri.split('/')[1]

  const extn = ext.split(';')[0]
  const buff = new Buffer(newUri, 'base64');
  const filename = uuid()
  const fullpath = `${savePath}/${filename}.${extn}`

  console.log('Saving to %o', fullpath)


  await fs.writeFileSync(fullpath, buff, function (err) {
    if (err) {
      return console.error(err);
    }
    console.log("The file was saved!");
  });

  console.log(`Base64 data converted to file: ${fullpath}`);
let thumb
  switch (extn) {
    case 'png':
      console.log(extn, 'Uploaded')
      thumb = `${savePath}/thumbs/` + prefix + filename + '.' + extn
      break;

    case 'pdf':
      console.log(extn, 'Uploaded')
      thumb = `${savePath}/thumbs/` + prefix + filename + '.' + 'jpg'
      console.log('Creating PDF %o', thumb)


      // const pdfImage = new PDFImage(fullpath);
      const pdfImage = new PDFImage(fullpath, {
        convertOptions: {
          "-resize": "200x200",
          "-quality": "75",
          "-write": thumb
        }
      });
      await pdfImage.convertPage(0).then(function (imagePath) {
        console.log(imagePath)
        // 0-th page (first page) of the slide.pdf is available as slide-0.png
        fs.existsSync("/tmp/slide-0.png") // => true
      });
      break;

    default:
      console.log(extn, 'Uploaded. Default Case')
      thumb = `${savePath}/thumbs/` + prefix + filename + '.' + extn
      break;
  }

  //Create image thumbnails with Sharp
  try {
    console.log('sharp', thumb)
    sharp(fullpath).resize(200, 200).toFile(thumb, (err, resizeImage) => {

      //Catch errors, create using no thumbnail
      if (err) {
        console.log(err);
        console.log('Failed to create thumbnail for %o%o', filename, extn)

        // generate image data to add to Images Database
        const newImage = new Image({
          UUID: filename,
          originalName: originalName,
          title: title,
          alt: filename,
          fileSrc: filename + '.' + extn,
          thumbSrc: filename + '.' + extn,
          fileExt: extn,
          fileSize: fileSize,
          uploader: uploader
        });

        //Save to images database
        newImage.save()
          .catch((error) => {
            res.send(error)
            console.log(error)
          })
          .then(() => {
            console.log(`Created ${newImage._id}`)
          })


        const objj = {
          UUID: filename,
          title: title,
          originalName: originalName,
          fileSrc: filename + '.' + extn,
          thumbSrc: 'uploads/' + filename + '.' + extn,
          fileSize: fileSize,
          uploader: uploader

        }
        console.log('objj', objj)

        //Append image to existing job id
        Job.findOneAndUpdate({
            _id: jobID
          }, {
            $push: {
              images: objj //TODO: replace with Image DB eventually
            }
          },
          function (error, success) {
            if (error) {
              // res.send(error.codeName)
              console.log(error);
            } else {
              console.log(`${filename}.${extn} Attached to ${jobID}`, );
            }
          });
      } else {

        // Handle success creating thumbnail 

        console.log(`Image thumbnail created`)
        // Add to image DB
        const newImage = new Image({
          UUID: filename,
          originalName: originalName,
          title: title,
          fileSrc: filename + '.' + extn,
          thumbSrc: thumb,
          fileExt: extn,
          fileSize: fileSize,
          uploader: uploader
        });

        newImage.save()
          .catch((error) => {
            res.send(error)
            console.log(error)
          })
          .then(() => {
            console.log(`Created ${newImage._id}`)
          })

        const imgId = {
          title: title,
          originalName: originalName,
          fileSrc: filename + '.' + extn,
          thumbSrc: thumb,

        }

        Job.findOneAndUpdate({
            _id: jobID
          }, {
            $push: {
              images: imgId //TODO: replace with Image._id eventually
            }
          },
          function (error, success) {
            if (error) {
              // res.send(error.codeName)
              console.log(error);
            } else {
              console.log('Updated Job');
            }
          });
      }
    })
  } catch (error) {
    console.error(error);
  }
}


module.exports = {

  list: async function (req, res) {

    const list = await Job.find({}).limit(15).sort(
      '-modified'
    ).cache();
    res.send(list)
  },

  listUsername: async function (req, res, next) {
    if (req.user.username) {

      const list = await Job.find({
        'assigned': req.user.username
      }).limit().sort(
        '-modified'
      ).cache();
      // console.log(list)
      res.send(list)
    } else {
      res.send('Error: Not logged in')
    }
  },

  notStarted: async function (req, res, next) {
    const list = await Job.find({
      'status': 'not-started'
    }).limit().sort({
      _id: 1
    }).cache();
    res.send(list)
  },

  recentJobs: async function (req, res, next) {
    const list = await Job.find({}).limit(10).sort({
      _id: -1
    }).cache();

    res.send(list)
  },

  read: function (req, res, next) {

    if (ObjectId.isValid(req.params.id)) {
      const job = Job.findOne({
        _id: req.params.id
      }, (err, data) => {
        if (err) {
          console.error(err)
          // return res.send('Job Not Found!');
        } else {
          return data
        }
      })
      .select("+images")
      .cache()

      res.send(job);
    }


  },

  //Destructuring is nice!
  update: async function (req, res, next) {
    await Job.findOneAndUpdate({
        "_id": ObjectId(req.params.id)
      }, {
        $set: {
          firstname: req.body.firstname,
          lastname: req.body.lastname,
          email: req.body.email,
          city: req.body.city,
          district: req.body.district,
          parts: req.body.parts,
          postcode: req.body.postcode,
          done: req.body.done,
          todo: req.body.todo,
          date: req.body.date,
          status: req.body.status,
          model: req.body.model,
          make: req.body.make,
          year: req.body.year,
          serial: req.body.serial,
          assigned: req.body.assigned,
          labourHours: req.body.labourHours,
          modified: new Date()
        }
      })
      .catch((error) => {
        console.error(error);
        res.send(401, error);
      })
      .then(() => {
        clearKey('jobs');
        res.send(`Modified Item ${req.params.id}`);

      })


    // res.send(`Updated ${req.params.id}`)
  },

  uploadImage: async function (req, res, next) {
    const obj = {
      'originalName': req.body.originalName,
      'title': req.body.title,
      'uri': req.body.image,
      'uploader': req.user.username,
      'filesize': req.body.image.length
    }
    if (obj.uri.length > 0) {
      //call Decode URI generate thumbs, add to db after
      await decode(res, req.body.id, obj).catch((e) => {
        console.error(e)
      }).then(() => {

        res.send('File Added')
      })
    } else {
      res.send('Data Error')
    }




  },

  notify: function (req, res, next) {
    //FireBase stuff

    const registrationToken = process.env.FIREBASE_KEY;

    const message = {
      data: {
        title: '850', // req.body.title
        body: '2:45' // req.body.message
      },
      token: registrationToken
    };

    admin.messaging().send(message)
      .then((response) => {
        console.log('Successfully sent message:', response);
        res.send('notified!')
      })
      .catch((error) => {
        console.log('Error sending message:', error);
        res.send(500, error)
      });

  },

  notifyTopic: function (req, res, next) {
    const message = {
      data: {
        title: '850',
        body: '2:45'
      },
      topic: req.params.id
    };
    admin.messaging().send(message)
      .then((response) => {
        // Response is a message ID string.
        console.log('Successfully sent message:', response);
        res.send(response)
      })
      .catch((error) => {
        console.log('Error sending message:', error);
        res.send(500, error)
      });

  },
  subscribe: function (req, res, next) {
    console.log(req.body)
    const registrationToken = req.body.currentToken
    admin.messaging().subscribeToTopic(registrationToken, req.params.id)
      .then(function (response) {
        console.log('Successfully subscribed to topic:', response);
        res.send(response)
      })
      .catch(function (error) {
        console.log('Error subscribing to topic:', error);
        res.send(500, error)
      });
  },

  unsubscribe: function (req, res, next) {
    console.log(req.body)
    const registrationToken = req.body.currentToken
    admin.messaging().unsubscribeFromTopic(registrationToken, req.params.id)
      .then(function (response) {
        console.log('Successfully unsubscribed from topic:', response);
        res.send(response)
      })
      .catch(function (error) {
        console.log('Error unsubscribing from topic:', error);
        res.send(500, error)
      });
  },

  create: async function (req, res, next) {
    console.log(`${req.reqUser} tried to create a job`)
    // console.log(req.body)

    const newJob = new Job({
      created: new Date(),
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      email: req.body.email,
      city: req.body.city,
      district: req.body.district,
      parts: req.body.parts,
      postcode: req.body.postcode,
      done: req.body.done,
      todo: req.body.todo,
      date: req.body.date,
      status: req.body.status,
      model: req.body.model,
      make: req.body.make,
      year: req.body.year,
      serial: req.body.serial,
      assigned: req.body.assigned,
      labourHours: req.body.labourHours
    });

    await newJob.save()
      .catch((error) => {
        console.log(error)
      })
      .then(() => {
        console.log('Created', newJob._id)
        // console.log(Job.collection)
        clearKey('jobs');
        res.send({
          message: `Created ${newJob._id}`,
          id: newJob.jobNumber
        })
      })

  },

  delete: function (req, res, next) {
    console.log(`${req.reqUser} deleted ${req.params.id} `)
    Job.findByIdAndRemove(req.params.id, (err, data) => {
      if (err) return res.send(500, err);
      console.log(data)
      const response = {
        message: `Deleted ${req.params.id}`,
      };
      clearKey('jobs');

      return res.send(response);
    });
  }
}