const Categories = require('../models/categories');
const Products = require('../models/products');
const {
    clearKey
} = require("../config/cache");

// Import ENV Variables from .env file
const dotenv = require('dotenv');
dotenv.config();

module.exports = {

    list: async function (req, res) {
        const list = await Categories.find({}).limit().sort({
            _id: 1
        }).cache({
            time: 600
        }).catch(err => {
            console.log(err)
        })
        res.send(list)
    },

    read: async function (req, res) {
        const list = await Products.find({
            category: req.params.id
        }).limit().sort({
            _id: -1
        }).cache({
            time: 600
        })
        // console.log(list)
        res.send(list)
    },
    readOne: async function (req, res) {
        // clearKey('categories');

        const list = await Categories.findOne({
            category: req.params.id
        }).limit().sort({
            _id: -1
        }).cache({
            time: 6000
        })
        // console.log(list)
        res.send(list)
    },


    create: async function (req, res, next) {
        res.send(200, 'ok')
        console.log(req.body)
        //TODO:



    },

    update: async function (req, res, next) {
//TODO:

    },

    delete: async function (req, res, next) {
//TODO:

    }
}