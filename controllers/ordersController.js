const mongoose = require('mongoose');

const orders = require('../models/orders');

const dotenv = require('dotenv');
dotenv.config();

const ObjectId = require('mongodb').ObjectID;

module.exports = {

  populate: async function (req, res) {
    // Used to create fake data
    //TODO:

  },

  list: async function (req, res) {

    // postsShow(req, res)

    await orders.Orders.find()
      // .populate('Customerid', 'Customer', 'Customerid')
      .exec((err, parts) => {
        console.log(parts);
        console.error(err)
        // return parts
        res.send(parts)
      });

    // res.send(list)
  },

  listAll: async function (req, res) {
    const limit = 3000;
    //const list = await ordersModel.find({}, ['partName', 'partNumber', 'price', 'thumbnail']).limit(Number(limit)).sort({
    //  _id: -1
    //});

    if (typeof req.params.page == 'undefined') {
      req.params.page = 1
    }

    const options = {
      page: req.params.page,
      limit: 10,
      collation: {
        locale: 'en'
      }
    };

    const list = await orders.Orders.paginate({},
        options)
      .then({})

    res.send(list)
  },

  read: function (req, res, next) {
    const job = ordersModel.findOne({
        _id: ObjectId(req.params.id)
      }, (err, data) => {
        if (err) {
          console.error(err)
          // return res.send('Job Not Found!');
        } else {
          return data
        }
      })
      .select("+images") // Images omitted in schema due to data uri's in db.

    res.send(job);
  },

  update: async function (req, res, next) {
    const {
      firstname,
      lastname,
    } = req.body;

    try {
      await ordersModel.findOneAndUpdate({
        "_id": ObjectId(req.params.id)
      }, {
        $set: {
          firstname: firstname,
          lastname: lastname,

        }
      }, function () {
        res.send(`Modified Item ${req.params.id}`);
      });
    } catch (error) {
      console.error(error);
      res.send(501, error);
    }


    // res.send(`Updated ${req.params.id}`)
  },

  create: function (req, res, next) {
    const {
      firstname,
      lastname,
      email,
    } = req.body;
    ordersModel.create({
      firstname: firstname,
      lastname: lastname,
      email: email,
    }, (err, data) => {
      if (err) {
        console.error(`Create error! ${err}`)
        return res.send(500, err);
      }

      const response = {
        message: `Successfully created ${data._id}`,
        id: `${data._id}`
      };
      return res.send(response);
    });
  },

  delete: function (req, res, next) {
    ordersModel.findByIdAndRemove(req.params.id, (err, data) => {
      if (err) return res.send(500, err);
      const response = {
        message: `${req.params.id} ${data}`,
      };
      return res.send(response);
    });
  }
}