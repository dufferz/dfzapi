const Products = require('../models/products');
// Import ENV Variables from .env file
const dotenv = require('dotenv');
const {
    ObjectID
} = require('mongodb');
dotenv.config();
const {
    clearKey
} = require("../config/cache");
const stripe = require('stripe')(process.env.STRIPE_KEY);

module.exports = {
    //List all products
    list: async function (req, res) {
        const list = await Products.find({}).limit().sort({
            _id: -1
        }).cache({
            time: 600 // cache result for 600s
        })
        res.send(list)
    },

//List all products by supplier/brand
    listBrand: async function (req, res) {
        const list = await Products.find({
            supplier: req.params.id
        }).limit().sort({
            _id: -1
        }).cache({
            time: 600 // cache result for 600s
        })
        res.send(list)
    },

    //read specific product
    read: async function (req, res) {
        if (req.params.id) {
            //replace %20 with " "
            item = req.params.id.replace(/%20/g, " ");
            const list = await Products.findOne({
                title: item
            }).limit().sort({
                _id: -1
            }).cache({
                time: 600 // cache result for 600s
            })
            // console.log(list)
            res.send(list)
        } else {
            res.send('No Item')
        }
    },

//Create new r
    create: async function (req, res, next) {

        //Create product on Stripe
        const product = await stripe.products.create({
            name: req.body.form.itemName,
            type: 'good',
            images: [`https://testing.dufferz.net/assets/images/products/${req.body.form.picture}.webp`]
        });

        let price;
        let sku;

        //Handle onsale/not on sale price creation on stripe
        if (req.body.form.onSale == true) {
            price = await stripe.prices.create({
                unit_amount: req.body.form.salePrice * 100,
                currency: 'nok',
                product: product.id,
            });
            sku = await stripe.skus.create({
                price: req.body.form.salePrice * 100,
                currency: 'nok',
                inventory: {
                    type: 'bucket',
                    value: 'in_stock'
                },
                product: product.id,
            });
        } else {
            price = await stripe.prices.create({
                unit_amount: req.body.form.price * 100,
                currency: 'nok',
                product: product.id,
            });
            sku = await stripe.skus.create({
                price: req.body.form.price * 100,
                currency: 'nok',
                inventory: {
                    type: 'bucket',
                    value: 'in_stock'
                },
                product: product.id,
            });
        }

        console.log(product.id, price.id, sku.id)

        //Add to db. Use destructuring!!

        Products.create({
            title: req.body.form.itemName,
            supplier: req.body.form.supplier,
            sku: req.body.form.sku,
            price: req.body.form.price,
            salePrice: req.body.form.salePrice,
            onSale: req.body.form.onSale,
            image: req.body.form.picture,
            images: req.body.form.images,
            specs: req.body.specs,
            description: req.body.form.description,
            category: req.body.form.category,
            stripeID: product.id,
            stripePriceID: price.id,
            stripeSKUID: sku.id,

            video: '', // Video unused RN, Angular embed errors
        }, (err, data) => {
            if (err) {
                console.log(`Create error! ${err}`)
                return res.send(err);
            } else {
                const response = {
                    message: `Successfully created ${data._id}`,
                    id: `${data._id}`
                };
                clearKey('products'); // Clear cache to display all new products on request
                return res.send(response);
            }
        });

    },

    update: async function (req, res, next) {

        // TODO:

    },

    delete: async function (req, res, next) {

        //TODO:

    }
}