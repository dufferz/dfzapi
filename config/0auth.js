const jwt = require('express-jwt');
const jwks = require('jwks-rsa');

const auth0Check = jwt({
    secret: jwks.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: 'https://dfz.eu.auth0.com/.well-known/jwks.json'
    }),
    audience: 'https://dfz.eu.auth0.com/api/v2/',
    issuer: 'https://dfz.eu.auth0.com/',
    algorithms: ['RS256']
});

// Check for an authenticated admin user
const adminCheck = (req, res, next) => {
    const roles = req.user['https://dfzservice.no/roles'] || [];
    if (roles.indexOf('admin') > -1) {
        next();
    } else {
        res.send(401, {
            message: 'Not authorized'
        });
    }
}
module.exports = {
    auth0Check,
    adminCheck
}