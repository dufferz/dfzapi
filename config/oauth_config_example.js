module.exports = {

  clientID: '',
  clientSecret: '',
  redirectURI: 'https://api.dufferz.net/oauth-callback',
  applicationID: '',

  // our FusionAuth api key
  apiKey: '',

  //ports
  clientPort: 4200,
  serverPort: 3000,
  fusionAuthPort: 9011
};