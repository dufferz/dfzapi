const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const chalk = require('chalk');

dotenv.config();

const verifyToken = async (req, res, next) => {
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  // console.log(req.cookies)

  const token = req.cookies.token || '';

  try {
    if (!token) {
      console.log(chalk.black.bgRed('A Token Check FAILED for', ip));
      res.send('Error! No Token!');
    } else {
      const decrypt = await jwt.verify(token, process.env.COOKIE_HASH);
      const time = new Date();
      user = {
        password: decrypt.password,
        username: decrypt.username,
      };
      req.reqUser = user.username;
      console.log(chalk.black.bgGreenBright(' -', time.toLocaleString(), '||', 'A Token Check Passed for:', req.reqUser + ' - '));
      next();
    }
  } catch (err) {
    console.error(err)
    return res.send(500, 'Some vague non descriptive error because debugging is for SysOps, not users');
  }
};

module.exports = verifyToken;