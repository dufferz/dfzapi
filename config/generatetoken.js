const jwt = require('jsonwebtoken');

const dotenv = require('dotenv');
dotenv.config();

const generateToken = (res, password, username, next) => {
    const expiration = process.env.DB_ENV === 'testing' ? 100 : 604800000;

    const token = jwt.sign({
        password,
        username
    }, process.env.COOKIE_HASH, {
        expiresIn: process.env.DB_ENV === 'testing' ? '1d' : '7d',
    });

    //Experimental function for seeing how secure and httponly cookies work VERY NASTY. DO NOT USE IN PROD. origin headers can lie!

    if (req.headers.origin == "https://lvh.me:3005") {
        res.cookie('token', token, {
            expires: new Date(Date.now() + expiration),
            secure: false, // set to true if your using https
            httpOnly: false,
            SameSite: 'None'
        });

    } else {
        res.cookie('token', token, {
            expires: new Date(Date.now() + expiration),
            secure: true, // set to true if your using https
            httpOnly: true,
            SameSite: 'None'
        });
    }


    res.send({
        success: true,
        msg: username + ' Signed in'
    });
};
module.exports = generateToken;