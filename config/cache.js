const mongoose = require("mongoose");
const redis = require("redis");
const util = require("util");
const moment = require('moment');
const chalk = require('chalk');
const io = require('@pm2/io')

//Dotenv
const dotenv = require('dotenv');
dotenv.config();


// PM2 Metrics for montitoring
const dbCount = io.meter({
  name: 'DB Requests/hour',
  samples: 600,
  timeframe: 600
})
const redisCount = io.meter({
    name: 'Redis Requests/hour',
    samples: 600,
    timeframe: 600
  })


// const keys = require("../config/keys");
// const client = redis.createClient(6379, 'localhost', {no_ready_check: true});

const client = redis.createClient({
    host: 'redis-master',
    port: 6379,
    no_ready_check: true,
    // auth_pass: process.env.REDIS_PW, 
    retry_strategy: () => 1000
});

//Handle redis errors
client.on('error', function (err) {
    console.error('Redis Error ' + err);
}); 

//Handle reconnect 
client.on('reconnecting', function (err) {
    console.error(chalk.blue(moment().format('h:mm:ss a')), chalk.red('Redis Error! Reconnecting!'));
});

//Show connected
client.on('connect', function() {
    console.log(chalk.blue(moment().format('h:mm:ss a')), chalk.green('Connected to Redis'));
});


//Cache config

client.hget = util.promisify(client.hget);
const exec = mongoose.Query.prototype.exec;

mongoose.Query.prototype.cache = function (options = {
    time: 600 // Default cache TTL N-secs
}) {
    this.useCache = true;
    this.time = options.time;
    this.hashKey = JSON.stringify(options.key || this.mongooseCollection.name);

    return this;
};

mongoose.Query.prototype.exec = async function () {
    if (!this.useCache) {
        return await exec.apply(this, arguments);
    }

    const key = JSON.stringify({
        ...this.getQuery()
    });

    const cacheValue = await client.hget(this.hashKey, key);

    // handle response from cache
    if (cacheValue) {
        const doc = JSON.parse(cacheValue);
        redisCount.mark() //pm2 metrics
        console.log("Response from Redis Cache");
        return Array.isArray(doc) ?
            doc.map(d => new this.model(d)) :
            new this.model(doc);
    }

    const result = await exec.apply(this, arguments);
    dbCount.mark() // pm2 metrics
    client.hset(this.hashKey, key, JSON.stringify(result));
    client.expire(this.hashKey, this.time);
    console.log("Response from MongoDB, Cache TTL: %o", this.time, this.hashKey);
    return result;
};

function clearKey(hashKey){
    console.log('Clearkey', hashKey)
    client.del(JSON.stringify(hashKey));
}

module.exports = {clearKey};