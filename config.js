const dotenv = require('dotenv');
dotenv.config();

jwt = {
  "jwt": {
    "secret": process.env.COOKIE_HASH
  }
}
module.exports=jwt