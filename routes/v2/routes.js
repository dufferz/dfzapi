const Router = require('restify-router').Router;
const router = new Router();
// Import Controllers

const Jobs = require('../../controllers/jobController');
const Parts = require('../../controllers/partsController');
const Images = require('../../controllers/imagesController');
const Orders = require('../../controllers/ordersController');
const Customers = require('../../controllers/customerController');
const Users = require('../../controllers/userController');
const Products = require('../../controllers/productsController');
const Categories = require('../../controllers/categoriesController');
const Payments = require('../../controllers/paymentsController');

//Import auth0 stuff
const {
  auth0Check,
  adminCheck
} = require('../../config/0auth');



// Base Routes
router.get('/', function (req, res, next) {
  res.send(`You shouldn't be here!`)
});

// Job Control Routes
router.get('/jobs/', Jobs.list);

router.get('/jobs/:username', Jobs.listUsername);
router.get('/jobs/notstarted', Jobs.notStarted);
router.get('/jobs/recentjobs', Jobs.recentJobs);
router.post('/job/:id/upload', Jobs.uploadImage)
router.get('/job/notify', Jobs.notify)
router.post('/job/:id/subscribe', Jobs.subscribe)
router.post('/job/:id/unsubscribe', Jobs.unsubscribe)
router.post('/job/:id/notify', Jobs.notifyTopic)
router.post('/job/create', Jobs.create)
router.get('/job/:id', Jobs.read);
router.post('/job/update/:id', Jobs.update)
router.del('/job/delete/:id', Jobs.delete)



// Parts Control Routes
router.get('/parts', Parts.paginate);
router.get('/parts/list/?limit=:id', Parts.paginate);
router.get('/parts/list/', Parts.listAll);
router.get('/parts/paginate/:page', Parts.paginate);
router.get('/parts/search/name/:query', Parts.searchName);
router.get('/parts/search/number/:query', Parts.searchNumber);
router.post('/parts/create', Parts.create)
router.get('/parts/:id', Parts.read);
router.post('/parts/update/:id', Parts.update)
router.del('/parts/delete/:id', Parts.delete)
router.post('/parts/:id/upload', Parts.uploadImage)


// Get Attachment from UUID
// router.get('/attachment:UUID', Images.findUUID); // List All Images (For admin use)
router.get('/attachment/:UUID', Images.findUUID); // List All Images (For admin use)



// Images / Attachments Control Routes
router.get('/images', Images.list); // List All Images (For admin use)
router.get('/images/:job', Images.read); // All Images for specific job
router.del('/images/:job/:id', Images.delete) // Remove images from job
router.post('/images/:job/upload', Images.uploadImage) // Upload Images to job


//TODO: Messaging Routes

// Order Details Routes (to use .populate())
router.get('/orders/', Orders.listAll);
router.post('/orders/create', Orders.create)
router.get('/orders/:id', Orders.list);
router.post('/orders/update/:id', Orders.update)
router.del('/orders/delete/:id', Orders.delete)



// Customer Details Routes (to use .populate())
router.get('/customers/', Customers.list);
router.post('/customers/create', Customers.create)
router.get('/customers/:id', Customers.read);
router.post('/customers/update/:id', Customers.update)
router.del('/customers/delete/:id', Customers.delete)




// User Routes
router.get('/users/list', Users.list)
router.post('/users/:user/updateToken', Users.updateToken)
router.post('/verifyToken', Users.verifyToken)
router.post('/refreshToken', Users.refreshToken)

router.post('/users/create', Users.create)
router.post('/users/:user/addtask', Users.addTask)
router.get('/users/:user/gettasks', Users.getTasks)


//Update Firebase Token 



// DFZ Repair Webstore Routes                         NOTE: be sure to check JWT exclusions

router.get('/products/', Products.list);
router.post('/products/create', auth0Check, adminCheck, Products.create)

router.get('/products/:id', Products.read);
router.post('/products/update/:id', Products.update)
router.del('/products/delete/:id', Products.delete)



router.get('/categories/', Categories.list);
router.post('/categories/create', auth0Check, adminCheck, Categories.create)
router.get('/categories/:id', Categories.read);
router.get('/category/:id', Categories.readOne);

router.post('/categories/update/:id', auth0Check, adminCheck, Categories.update)
router.del('/categories/delete/:id', auth0Check, adminCheck, Categories.delete)



// Payments section, Consider moving to another file

router.post('/payments/:id', auth0Check, Payments.create)

router.get('/payments/list', auth0Check, adminCheck, Payments.readAll)

//router.get('/payments/customerinfo/:id',auth0Check, adminCheck, Payments.readCustomer)
//router.get('/payments/paymentinfo/:id',auth0Check, adminCheck, Payments.readIntent)

router.post('/payments/listorders', auth0Check, Payments.readUser)

// router.post('/payments/readsession',auth0Check, Payments.readSession)

router.post('/stripeorder', auth0Check, Payments.read)

//Admin role locked payment user.role==admin
router.post('/payment/:id', auth0Check, adminCheck, Payments.readOne)



router.get('/brands/:id', Products.listBrand);


router.post('/stripe', function (req, res, next) {
  console.log(req.body.data)
  if (req.body && req.body.data && req.body.id) {
    Payments.createOrder(req.body.data.object, req.body.id)
    res.send('ok')
  } else {
    res.send(500, 'Malformed Data')
  }


})

module.exports = router;